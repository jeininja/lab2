//Jei wen Wu -1842614 

public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] bicycles = new Bicycle[4];
		bicycles[0] = new Bicycle("Argon", 18, 22.2);
		bicycles[1] = new Bicycle("Banshee", 20, 24.4);
		bicycles[2] = new Bicycle("Cachet", 22, 26.6);
		bicycles[3] = new Bicycle("Dino", 24, 28.8);
		
		for(Bicycle bicycle : bicycles) {
			System.out.println(bicycle.toString());
		}
	}

}
