//Jei wen Wu -1842614

public class Bicycle {
String manufacturer;
int numberGears;
double maxSpeed;

public String getManufacturer() {
	return this.manufacturer;
}

public int getNumberGears() {
	return this.numberGears;
}

public double getMaxSpeed() {
	return this.maxSpeed;
}

public Bicycle(String manufacturer, int numberGears, double maxSpeed)
{
	this.manufacturer = manufacturer;
	this.numberGears = numberGears;
	this.maxSpeed = maxSpeed;
}

public String toString() {
	String temp = "Manufacturer: "+getManufacturer()+", Number of Gears: "+getNumberGears()+", MaxSpeed: "+getMaxSpeed();
	return temp;
}
}
